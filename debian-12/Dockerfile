FROM debian:bookworm
MAINTAINER Ansgar.Burchardt@tu-dresden.de
RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes \
  && rm -rf /var/lib/apt/lists/*
RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  autoconf \
  automake \
  bison \
  build-essential \
  codespell \
  ca-certificates \
  clang-14 \
  libc++-14-dev \
  libc++abi-14-dev \
  cmake \
  coinor-libipopt-dev \
  cppcheck \
  curl \
  flex \
  g++ g++-12 \
  gcc gcc-12 \
  gcovr \
  gfortran gfortran-12 \
  git git-lfs \
  lcov \
  libadolc-dev \
  libalberta-dev \
  libarpack++2-dev \
  libboost-dev \
  libboost-program-options-dev \
  libboost-serialization-dev \
  libboost-system-dev \
  libeigen3-dev \
  libffi-dev \
  libfftw3-dev \
  libgmp-dev \
  libgtest-dev \
  libhdf5-dev \
  libisl-dev \
  libltdl-dev \
  libmpfr-dev \
  libmpfrc++-dev \
  libmuparser-dev \
  libopenblas-dev \
  libscotchmetis-dev \
  libscotchparmetis-dev \
  libsuitesparse-dev \
  libsuperlu-dev \
  libtbb-dev \
  libtinyxml2-dev \
  libtiff-dev \
  libtool \
  libtrilinos-zoltan-dev \
  locales-all \
  mpi-default-bin \
  mpi-default-dev \
  nano \
  ninja-build \
  openssh-client \
  pkg-config \
  petsc-dev \
  python3 \
  python3-dev \
  python3-matplotlib \
  python3-mpi4py \
  python3-numpy \
  python3-petsc4py \
  python3-pip \
  python3-pytest \
  python3-setuptools \
  python3-scipy \
  python3-venv \
  python3-vtk9 \
  python3-ufl \
  python3-wheel \
  uncrustify \
  vc-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# workaround for a problem with openmpi in debian-11. See docker/ci #18
ENV OMPI_MCA_pml="^ucx"
RUN adduser --disabled-password --home /duneci --uid 50000 duneci

RUN python3 -m venv /duneci/venv
RUN /duneci/venv/bin/pip3 install codechecker

# The environment for Dune builds is set up in base-common/dune-setup.dockerfile,
# which gets appended to this file during the build process

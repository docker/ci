Docker images for GitLab CI
===========================

Images
------

The current images are provided in the registry, `registry.dune-project.org/docker/ci/...`,
and are mirrored in the docker hub, `duneci/...`:

### Base Images

The base images all offer a set of _toolchains_. You can select a toolchain by defining the
job variable `DUNECI_TOOLCHAIN`. The corresponding opts file is always `/duneci/dune.opts`,
but that will be automatically picked up by the scripts in the image. Toolchains are given
by strings of the following format: `<compiler>-<version>-<optional extras>-<c++ standard>`.

| image                      | description                        | toolchains |
|----------------------------|-----------------------------------|------------|
| [debian:10](registry.dune-project.org/docker/ci/debian:10)    | Debian 10 with cmake 3.11 | **gcc-7-14**, gcc-7-noassert-14, gcc-7-17, gcc-8-17, gcc-8-noassert-17, clang-6-17, clang-6-noassert-17, clang-7-libcpp-17, clang-7-libcpp-noassert-17 |
| [debian:11](registry.dune-project.org/docker/ci/debian:11)    | Debian 11 with cmake 3.16 | gcc-9-17, gcc-9-20, gcc-9-noassert-20, **gcc-10-20**, gcc-10-noassert, clang-11-20, clang-13-20-libcxx |
| [debian:12](registry.dune-project.org/docker/ci/debian:12)    | Debian 12 (bookworm) with cmake 3.24 | **gcc-12-20**, gcc-12-noassert-20, clang-14-20, clang-14-20-libcxx, clang-14-noassert-20 |
| [ubuntu:18.04](registry.dune-project.org/docker/ci/ubuntu:18.04) | Ubuntu LTS 18.04 with updated cmake 3.17 | **gcc-7-14**, gcc-7-noassert-14, gcc-7-17, gcc-7-noassert-17, clang-5-17, clang-6-17, clang-6-noassert-17 |
| [ubuntu:20.04](registry.dune-project.org/docker/ci/ubuntu:20.04) | Ubuntu LTS 20.04 with cmake 3.16         | **gcc-9-17**, gcc-9-20, gcc-9-noassert-17, gcc-9-noassert-20, gcc-10-20, clang-10-17, clang-10-noassert-17, clang-10-20, clang-10-noassert-20 |
| [ubuntu:23.04](registry.dune-project.org/docker/ci/ubuntu:23.04) | Ubuntu 23.04 with cmake 3.25         | **gcc-13-20**, gcc-13-23, clang-16-20, clang-16-23, clang-18-23, clang-18-libcpp-23   |
| [ubuntu:24.04](registry.dune-project.org/docker/ci/ubuntu:24.04) | Ubuntu LTS 24.04 with cmake 3.28         | **gcc-14-23**, gcc-14-26, clang-18-23, clang-18-libcpp-23, clang-18-26, clang-18-libcpp-26   |

### Core Images

These images contain the core and staging modules compiled for several combinations of distributions and toolchains. The toolchain is locked
in for these images. See https://gitlab.dune-project.org/docker/ci/container_registry/46 for a list of possible setups.

There is also some default configurations for each version:

| image                  | distribution | toolchain |
|------------------------|--------------|-----------|
| [dune:2.7](registry.dune-project.org/docker/ci/dune:2.7) | debian-10    | gcc-8-17  |
| [dune:2.8](registry.dune-project.org/docker/ci/dune:2.8) | debian-11    | gcc-9-20  |
| [dune:2.9](registry.dune-project.org/docker/ci/dune:2.9) | debian-11    | gcc-10-20  |
| [dune:2.9](registry.dune-project.org/docker/ci/dune:2.10)| debian-11    | gcc-10-20  |
| [dune:git](registry.dune-project.org/docker/ci/dune:git) | debian-12    | gcc-12-20  |


### Module Dependency Images

These images contain all dependencies for testing the respective downstream module. They are mirrored in a similar way to the images listed above.
Find the list of these images in https://gitlab.dune-project.org/docker/ci/container_registry/47.


### Module Images

These images are for testing downstream modules: https://gitlab.dune-project.org/docker/ci/container_registry/49

`.gitlab-ci.yml`
----------------

Installing dependencies:
```yaml
before_script:
  - duneci-install-module https://gitlab.dune-project.org/core/dune-common.git
  - duneci-install-module https://gitlab.dune-project.org/core/dune-geometry.git
```

To build with several images:
```yaml
---
dune:2.7--gcc:
  image: registry.dune-project.org/docker/ci/dune:2.7
  script: duneci-standard-test

dune:2.7--clang:
  image: registry.dune-project.org/docker/ci/dune:2.7
  script: duneci-standard-test --opts=/duneci/opts.clang
```

You can also specify a default image and use it in several jobs:

```yaml
---
image: registry.dune-project.org/docker/ci/dune:2.7

dune:2.7--gcc:
  script: duneci-standard-test

dune:2.7--clang:
  script: duneci-standard-test --opts=/duneci/opts.clang
```

For more information, take a look at the [GitLab documentation on `.gitlab-ci.yml`](https://docs.gitlab.com/ce/ci/yaml/README.html).

Running the pipelines
---------------------

Images are created by GitLab CI pipelines.  You can run these pipelines manually from the GitLab interface.  To do this, click Main Menu -> Build -> Pipelines -> Run Pipeline. By default, this will run the pipeline/build the images for the Dune master branch.

You can also specify which version to build. To do this, write the variable name `TRIGGER_VERSION` in the `Input variable key` field and set the corresponding value field to one of `2.7`, `2.8`, `2.9`, `2.10`, or `git`.

Debugging within a docker image
-------------------------------

Sometimes it is useful to be able to debug Dune inside one of the Docker images created here. The main motivation is that sometimes the test suites of the various Dune modules only show failures under certain circumstances, i.e. with certain images.  The easiest way to debug such a failure is inside the container itself.

There are at least two ways to do this.

### Running a CI pipeline locally

Install the [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local) tool.

You can use the tool by running

```shell
  gitlab-ci-local --variable HOME=/duneci <job>
```
in your Dune module source directory.  Here, `<job>` is one build job name from the file `.gitlab-ci.yml` of the module that you want to test.

For example, to run a pipeline for the `dune-common` module go to the main directory of the `dune-common` source tree and run

```shell
  gitlab-ci-local --variable HOME=/duneci "debian:11 clang-11-20-nobindings"
```
This will run the job using the `debian:11` base image with the `clang-11` compiler set to C++20, and without the Python bindings.`

See also this [stackoverflow entry](https://stackoverflow.com/questions/32933174/use-gitlab-ci-to-run-tests-locally).

### Debugging interactively within the container

To start an interactive debugging system within one of the Docker images used for testing run

```shell
  docker run -it --rm <image>
```
with `<image>` one of the Dune docker images from our registry. This starts a shell within the container.  There, you first need to set all environment variables of the form `DUNECI_xxx` that are also set in the `.gitlab-ci.yml` file of the module you want to debug.  For example, consider the job `debian:11 clang-11-20-nobindings` from the `.gitlab-ci.yml` in dune-common:

```yml
debian:11 clang-11-20-nobindings:
  extends: .common
  image: registry.dune-project.org/docker/ci/debian:11
  variables:
    DUNECI_TOOLCHAIN: clang-11-20
    DUNECI_CMAKE_FLAGS: '-DDUNE_ENABLE_PYTHONBINDINGS=OFF'
```

the `<image>` is `registry.dune-project.org/docker/ci/debian:11` and we have to set two environmental variables,

```shell
  export DUNECI_TOOLCHAIN="clang-11-20"
  export DUNECI_CMAKE_FLAGS="-DDUNE_ENABLE_PYTHONBINDINGS=OFF"
```

Then type

```shell
  source /duneci/bin/duneci-init-job
```

to load the selected toolchain and to set some additional environmental variables used in the ci jobs.

Then download and build the source code for the Dune module in question:

```shell
  duneci-install-module <module-url>
```
with `<module-url>` the git url used to clone the Dune module, e.g.,

```shell
  duneci-install-module https://gitlab.dune-project.org/core/dune-common.git
```

Modules are "installed" (this means downloaded and then build using `dunecontrol`) in the directory `/dunci/modules/[my-module]`. For running tests or investigating the build output, enter the Dune module's source directory, e.g,

```shell
  cd /duneci/modules/dune-common
```

... and run the tests:

```shell
  duneci-standard-test
```

Adding new images
-----------------

In order to add a new image, create a directory with the Dockerfile and any additional files that you want to have available in the Docker
build context. The naming scheme of the directory has to be `<image name>-<tag>`. Then edit `.gitlab-ci.yml` and add an appropriate entry to
the list of images in the correct stage (base / core / modules). If you want to have the image mirrored to [Docker Hub](https://hub.docker.com/),
talk to Ansgar or Steffen; they have to create the repository on the hub before that works.

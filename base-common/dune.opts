#!/bin/bash

if [[ $(basename $0) == dune.opts ]] ; then
    # we are in script mode, let's be strict here
    set -e
    set -u
    opts_mode=script
else
    opts_mode=opts
fi

if [[ -L ~/toolchain ]] ; then

    toolchain=$(basename $(readlink ~/toolchain))

    if [[ -v DUNECI_TOOLCHAIN ]] ; then
        echo 1>&2 "Image has already selected toolchain ${toolchain}, cannot override with DUNECI_TOOLCHAIN=${DUNECI_TOOLCHAIN}"
        exit 1
    fi

elif [[ -v DUNECI_TOOLCHAIN ]] ; then

    toolchain=${DUNECI_TOOLCHAIN}

    if [[ ! -f ~/toolchains/${toolchain} ]] ; then
        echo 1>&2 "Unknown toolchain selected: ${DUNECI_TOOLCHAIN}"
        exit 1
    fi

else

    toolchain=$(<~/toolchains/default)

fi

source ~/toolchains/${toolchain}

CMAKE_FLAGS=""
CMAKE_FLAGS+=" -DCMAKE_C_COMPILER=$CC"
CMAKE_FLAGS+=" -DCMAKE_CXX_COMPILER=$CXX"
CMAKE_FLAGS+=" -DCMAKE_C_FLAGS='${CFLAGS} ${DUNECI_CFLAGS:-}'"
CMAKE_FLAGS+=" -DCMAKE_CXX_FLAGS='${CXXFLAGS} ${DUNECI_CXXFLAGS:-}'"
CMAKE_FLAGS+=" -DCMAKE_BUILD_TYPE=${DUNECI_BUILD_TYPE:-RelWithDebInfo}"
CMAKE_FLAGS+=" ${DUNECI_PARALLEL:+-DDUNE_MAX_TEST_CORES=${DUNECI_PARALLEL}}"
CMAKE_FLAGS+=" -DCMAKE_INSTALL_PREFIX=/duneci/install"

# disable auto-detection of CPU features by default (for Vc)
CMAKE_FLAGS+=" -DTARGET_ARCHITECTURE=generic"

# this is needed e.g. to enable search in dune's gitlab registry to find python packages
# pypi is not accessible in CI
CMAKE_FLAGS+=" -DDUNE_RUNNING_IN_CI=ON"

# include flags from toolchain
CMAKE_FLAGS+=" ${TOOLCHAIN_CMAKE_FLAGS}"

# include flags from image
for file in ~/cmake-flags/* ; do
    source ${file}
done

# include flags from job
CMAKE_FLAGS+=" ${DUNECI_CMAKE_FLAGS:-}"


if [[ ${opts_mode} == script ]] ; then
    echo "Running with toolchain: ${toolchain}"
    echo "CMAKE_FLAGS=\"${CMAKE_FLAGS}\""
fi

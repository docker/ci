FROM ubuntu:18.04
MAINTAINER simon.praetorius@tu-dresden.de
RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes \
  && rm -rf /var/lib/apt/lists/*

# install some essential packages
RUN export DEBIAN_FRONTEND=noninteractive; \
  apt-get update \
  && apt-get dist-upgrade --no-install-recommends --yes \
  && apt-get install --no-install-recommends --yes \
  ca-certificates \
  gcc g++ git \
  make \
  wget \
  && apt-get clean

# download all packages that can be installed later
RUN apt-get install --reinstall --download-only --yes \
  mpi-default-bin \
  mpi-default-dev \
  pkg-config \
  python3 \
  python3-dev \
  python3-venv \
  python3-pip \
  python3-mpi4py

# download and install cmake in version 3.13
RUN wget -O /tmp/cmake-3.13.0.sh https://github.com/Kitware/CMake/releases/download/v3.13.0/cmake-3.13.0-Linux-x86_64.sh \
  && yes | bash /tmp/cmake-3.13.0.sh --prefix=/usr/local --skip-license --exclude-subdir \
  && rm /tmp/cmake-3.13.0.sh

RUN adduser --system --disabled-password --home /duneci --uid 50000 duneci

# The environment for Dune builds is set up in base-common/dune-setup.dockerfile,
# which gets appended to this file during the build process

import subprocess
import os
from distutils.dir_util import copy_tree

registry = "registry.dune-project.org/docker/ci"
releases = {
  "dune-2.8": {
    "dune:2.8-debian-10-gcc-8-17": {
      "BASE_IMAGE": "debian:10",
      "TOOLCHAIN": "gcc-8-17"
    },
    "dune:2.8-debian-11-gcc-9-20": {
      "BASE_IMAGE": "debian:11",
      "TOOLCHAIN": "gcc-9-20",
      "EXTRA_TAG": "2.8"
    },
    "dune:2.8-debian-11-gcc-10-20": {
      "BASE_IMAGE": "debian:11",
      "TOOLCHAIN": "gcc-10-20"
    }
  },
  "dune-2.9": {
    "dune:2.9-debian-10-gcc-8-17": {
        "BASE_IMAGE": "debian:10",
        "TOOLCHAIN": "gcc-8-17"
    },
    "dune:2.9-debian-11-gcc-10-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "gcc-10-20",
        "EXTRA_TAG": "2.9"
    },
    "dune:2.9-debian-11-clang-11-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "clang-11-20"
    }
  },
  "dune-2.10": {
    "dune:2.10-debian-11-gcc-10-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "gcc-10-20",
        "EXTRA_TAG": "2.10"
    },
    "dune:2.10-debian-11-clang-11-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "clang-11-20"
    },
    "dune:2.10-ubuntu-20.04-gcc-9-17": {
        "BASE_IMAGE": "ubuntu:20.04",
        "TOOLCHAIN": "gcc-9-17"
    },
    "dune:2.10-ubuntu-20.04-clang-10-17": {
        "BASE_IMAGE": "ubuntu:20.04",
        "TOOLCHAIN": "clang-10-17"
    }
  },
  "dune-git": {
    "dune:git-debian-11-gcc-10-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "gcc-10-20"
    },
    "dune:git-debian-11-clang-13-20": {
        "BASE_IMAGE": "debian:11",
        "TOOLCHAIN": "clang-13-20"
    },
    "dune:git-debian-12-gcc-12-20": {
        "BASE_IMAGE": "debian:12",
        "TOOLCHAIN": "gcc-12-20",
        "EXTRA_TAG": "git"
    }
  }
}

for sourcedir,jobs in releases.items():
  for job,vars in jobs.items():
    image = registry + "/" + job
    print(image)
    builddir = os.path.join("build/", job)
    if not os.path.exists(builddir):
      os.makedirs(builddir)

    copy_tree(sourcedir, builddir)

    docker_opts = []
    if "TOOLCHAIN" in vars:
      docker_opts.append("--build-arg TOOLCHAIN=" + vars["TOOLCHAIN"])

    if "BASE_IMAGE" in vars:
      base_image = registry + "/" + vars["BASE_IMAGE"]
      docker_opts.append("--build-arg BASE_IMAGE=" + base_image)

    print(' '.join(["docker", "build", "-t "+image] + docker_opts + [builddir]))
    subprocess.run(' '.join(["docker", "build", "-t "+image] + docker_opts + [builddir]), shell=True)
    subprocess.run(' '.join(["docker", "push", image]), shell=True)

    if "EXTRA_TAG" in vars:
      extra_image = registry + "/" + job.split(':')[0] + ":" + vars["EXTRA_TAG"]
      print(extra_image)
      print(' '.join(["docker", "tag", image, extra_image]))
      subprocess.run(' '.join(["docker", "tag", image, extra_image]), shell=True)
      subprocess.run(' '.join(["docker", "push", extra_image]), shell=True)

import subprocess
import os
from distutils.dir_util import copy_tree

registry = "registry.dune-project.org/docker/ci"
bases = {
  "debian-10": "debian:10",
  "debian-11": "debian:11",
  "debian-12": "debian:12",
  "ubuntu-18.04": "ubuntu:18.04",
  "ubuntu-20.04": "ubuntu:20.04",
  "ubuntu-22.04": "ubuntu:22.04",
  "ubuntu-23.04": "ubuntu:23.04",
  "ubuntu-24.04": "ubuntu:24.04"
}

for sourcedir,job in bases.items():
  image = registry + "/" + job
  print(image)
  builddir = os.path.join("build/", job)
  if not os.path.exists(builddir):
    os.makedirs(builddir)

  copy_tree("base-common/", builddir)
  copy_tree(sourcedir, builddir)

  # append dune-setup.dockerfile to Dockerfile
  with open(builddir + "/Dockerfile", "+a") as outFile:
    with open(builddir + "/dune-setup.dockerfile", "r") as inFile:
      for line in inFile:
        outFile.write(line)

  docker_opts = []

  print(' '.join(["docker", "build", "-t "+image] + docker_opts + [builddir]))
  subprocess.run(' '.join(["docker", "build", "-t "+image] + docker_opts + [builddir]), shell=True)
  subprocess.run(' '.join(["docker", "push", image]), shell=True)